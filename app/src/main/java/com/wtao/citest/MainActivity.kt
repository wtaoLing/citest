package com.wtao.citest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.coroutines.*
import kotlin.math.log

class MainActivity : AppCompatActivity() {
    companion object {
        private const val TAG = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.e("wtao", "测试打包1111")

        GlobalScope.launch {
            val job = withTimeout(1300) {
                repeat(100) {
                    println("I'm sleeping $it ...")
                    delay(500)
                }
                "Done"
            }
            delay(2000)
            println("I'm run complete $job")
        }
    }

    private fun test1() {
        runBlocking {
            GlobalScope.launch {
                delay(1000)
                Log.e("wtao", "world!")
            }
            Log.e("wtao", "hello ")
        }
    }

    private suspend fun job() {
        val job = GlobalScope.launch {
            delay(2000)
            Log.e(TAG, "world!")
        }
        Log.e(TAG, "hello!")
        job.join()
        Log.e(TAG, "complete")
    }

    private fun test2() = runBlocking { // this: CoroutineScope
        launch {
            delay(200L)
            println("Task from runBlocking")
        }

        coroutineScope { // 创建一个协程作用域
            launch {
                delay(500L)
                println("Task from nested launch")
            }

            delay(100L)
            println("Task from coroutine scope") // 这一行会在内嵌 launch 之前输出
        }

        println("Coroutine scope is over") // 这一行在内嵌 launch 执行完毕后才输出
    }

    private fun cancelTest() = GlobalScope.launch {
        println("main: I'm tired of waiting!")
        val job = launch {
            repeat(1000) {
                println("job: I'm sleeping $it ...")
                delay(500)
            }
        }
        delay(4000)
        job.cancelAndJoin()
        println("main: Now I can quit.")
    }

    private fun println(str: String) {
        Log.e(TAG, str)
    }
}